<?php

declare(strict_types=1);

$config = require_once __DIR__.'/.php_cs_config';
$finder = PhpCsFixer\Finder::create()->in([__DIR__.'/src', __DIR__.'/tests']);

return $config->setFinder($finder);
