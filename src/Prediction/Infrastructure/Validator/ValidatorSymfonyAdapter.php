<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Validator;

use App\Prediction\Application\Request\Request;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Errors\ValidationToErrors;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorSymfonyAdapter implements Validator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ValidationToErrors
     */
    private $validationToErrors;

    public function __construct(
        ValidatorInterface $validator,
        ValidationToErrors $validationToErrors
    ) {
        $this->validator = $validator;
        $this->validationToErrors = $validationToErrors;
    }

    public function validate(Request $request, Errors $errors): bool
    {
        $validation = $this->validator->validate($request);
        $validationErrors = $this->validationToErrors->toErrors($validation);
        if (false === $validationErrors->isEmpty()) {
            $errors->merge($validationErrors);
            return false;
        }

        return true;
    }
}
