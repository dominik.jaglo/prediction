<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Repository;

use App\Prediction\Domain\Model\Prediction;
use Doctrine\ODM\MongoDB\DocumentRepository;

class PredictionRepositoryDoctrineODM extends DocumentRepository implements \App\Prediction\Domain\Repository\PredictionRepository
{
    public function save(Prediction $prediction): void
    {
        $this->getDocumentManager()->persist($prediction);
    }

    public function findById(int $id): ?Prediction
    {
        return $this->find($id);
    }

    /**
     * @return Prediction[]
     */
    public function findAll(): array
    {
        return parent::findAll();
    }
}
