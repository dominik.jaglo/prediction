<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Service;

use App\Prediction\Application\ApplicationErrors;
use App\Prediction\Application\Model\Json;
use App\Prediction\Domain\Model\Errors;
use Symfony\Component\HttpFoundation\Request;

class JsonFactory
{
    public function fromRequest(Request $request, Errors $errors): ?Json
    {
        if ('json' !== $request->getContentType()) {
            $errors->addError(ApplicationErrors::JSON_CONTENT_TYPE);
            return null;
        }

        $data = $request->getContent(false);
        $data = json_decode($data, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            $errors->addError(ApplicationErrors::JSON_INVALID_CONTENT);
            return null;
        }

        if (false === is_array($data)) {
            $errors->addError(ApplicationErrors::JSON_CONTENT_MUST_BE_AN_ARRAY);
            return null;
        }

        return new Json($data);
    }
}
