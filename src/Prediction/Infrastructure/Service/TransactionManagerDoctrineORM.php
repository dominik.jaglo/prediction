<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Service;

use App\Prediction\Application\Service\TransactionManager;
use Doctrine\ODM\MongoDB\DocumentManager;

class TransactionManagerDoctrineORM implements TransactionManager
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function commit(): void
    {
        $this->documentManager->flush();
    }
}
