<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Http;

use App\Prediction\Domain\Model\Errors;
use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorsResponse extends JsonResponse
{
    public static function fromErrors(Errors $errors): self
    {
        return static::create(['errors' => $errors->toArray()], self::HTTP_BAD_REQUEST);
    }
}
