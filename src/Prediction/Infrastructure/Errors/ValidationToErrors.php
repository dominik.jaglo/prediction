<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Errors;

use App\Prediction\Domain\Model\Errors;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationToErrors
{
    public function toErrors(ConstraintViolationListInterface $list): Errors
    {
        $errors = new Errors();
        /** @var ConstraintViolationInterface $item */
        foreach ($list as $item) {
            $errors->addError($item->getPropertyPath().' - '.$item->getMessage());
        }
        return $errors;
    }
}
