<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Test;

use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\ValueObject\EventId;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Domain\ValueObject\PredictionValue;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use MongoClient;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use MockeryPHPUnitIntegration;

    public static function assertErrorsHaveMessage(string $message, Errors $errors): void
    {
        foreach ($errors->toArray() as $error) {
            if ($error['message'] === $message) {
                static::assertTrue(true);
                return;
            }
        }

        static::assertTrue(false, "Errors do not contain message: [{$message}]");
    }

    protected static function resetMongo(): void
    {
        $client = new MongoClient(getenv('MONGODB_URL'));
        $client->Prediction->Prediction->drop();
        $client->Prediction->doctrine_increment_ids->drop();
    }

    protected static function createPrediction(): Prediction
    {
        $errors = new Errors();

        return Prediction::create(
            EventId::fromInt(1, $errors),
            MarketType::drawOrWin(),
            PredictionValue::fromString('X', $errors),
            $errors
        );
    }

    protected static function request(string $method, string $uri, array $data = [], string $contentType = 'application/json'): Response
    {
        $client = new Client();
        return $client->request($method, $uri, [
            'body' => json_encode($data),
            'headers' => ['Content-Type' => $contentType],
            'http_errors' => false,
        ]);
    }

    protected static function extractResponseData(Response $response): array
    {
        return json_decode((string) $response->getBody(), true);
    }
}
