<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Controller;

use App\Prediction\Application\Handler\CreatePredictionHandler;
use App\Prediction\Application\Handler\UpdateStatusHandler;
use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Application\Service\ErrorsFactory;
use App\Prediction\Application\Service\PredictionListing;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Repository\PredictionRepository;
use App\Prediction\Infrastructure\Http\ErrorsResponse;
use App\Prediction\Infrastructure\Service\JsonFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PredictionController
{
    /**
     * @var PredictionListing
     */
    private $listing;

    /**
     * @var CreatePredictionHandler
     */
    private $createPredictionHandler;

    /**
     * @var UpdateStatusHandler
     */
    private $updateStatusHandler;

    /**
     * @var ErrorsFactory
     */
    private $errorsFactory;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PredictionRepository
     */
    private $repository;

    public function __construct(
        PredictionListing $listing,
        PredictionRepository $repository,
        CreatePredictionHandler $createPredictionHandler,
        UpdateStatusHandler $updateStatusHandler,
        ErrorsFactory $errorsFactory,
        JsonFactory $jsonFactory,
        LoggerInterface $logger
    ) {
        $this->listing = $listing;
        $this->createPredictionHandler = $createPredictionHandler;
        $this->updateStatusHandler = $updateStatusHandler;
        $this->errorsFactory = $errorsFactory;
        $this->jsonFactory = $jsonFactory;
        $this->logger = $logger;
        $this->repository = $repository;
    }

    public function getPredictions(): JsonResponse
    {
        $collection = $this->listing->getListing();
        return new JsonResponse(['results' => $collection->toArray()]);
    }

    public function createPrediction(Request $request): JsonResponse
    {
        $errors = $this->errorsFactory->create();
        $json = $this->jsonFactory->fromRequest($request, $errors);
        if (false === $errors->isEmpty()) {
            return $this->createErrorsResponse($errors);
        }

        $this->createPredictionHandler->handle(CreatePredictionRequest::create($json), $errors);

        if (false === $errors->isEmpty()) {
            return $this->createErrorsResponse($errors);
        }

        return new JsonResponse([], JsonResponse::HTTP_CREATED);
    }

    public function updateStatus($id, Request $request): JsonResponse
    {
        $prediction = $this->repository->find($id);
        if (null === $prediction) {
            return new JsonResponse([], JsonResponse::HTTP_NOT_FOUND);
        }

        $errors = $this->errorsFactory->create();
        $json = $this->jsonFactory->fromRequest($request, $errors);
        if (false === $errors->isEmpty()) {
            return $this->createErrorsResponse($errors);
        }

        $this->updateStatusHandler->handle($prediction, UpdateStatusRequest::create($json), $errors);
        if (false === $errors->isEmpty()) {
            return $this->createErrorsResponse($errors);
        }

        return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
    }

    private function createErrorsResponse(Errors $errors): ErrorsResponse
    {
        $this->logErrors($errors);
        return ErrorsResponse::fromErrors($errors);
    }

    private function logErrors(Errors $errors): void
    {
        $messages = array_map(function (array $data) {
            return sprintf('[%s]', $data['message']);
        }, $errors->toArray());
        $this->logger->debug(implode(' | ', $messages));
    }
}
