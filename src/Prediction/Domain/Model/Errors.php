<?php

declare(strict_types=1);

namespace App\Prediction\Domain\Model;

class Errors
{
    /**
     * @var array
     */
    private $errors;

    public function __construct()
    {
        $this->errors = [];
    }

    public function addError(string $message): void
    {
        $this->errors[] = ['message' => $message];
    }

    public function toArray(): array
    {
        return $this->errors;
    }

    public function isEmpty(): bool
    {
        return empty($this->errors);
    }

    public function merge(Errors $errors): void
    {
        foreach ($errors->toArray() as $error) {
            $this->addError($error['message']);
        }
    }
}
