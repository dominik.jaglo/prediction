<?php

declare(strict_types=1);

namespace App\Prediction\Domain\Model;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\ValueObject\EventId;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Domain\ValueObject\PredictionValue;
use App\Prediction\Domain\ValueObject\Status;

class Prediction
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var EventId
     */
    private $event;

    /**
     * @var MarketType
     */
    private $marketType;

    /**
     * @var PredictionValue
     */
    private $predictionValue;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var int
     */
    private $updatedAt;

    public static function create(
        EventId $event,
        MarketType $marketType,
        PredictionValue $predictionValue,
        Errors $errors
    ): ?self {
        if (
            $marketType->isCorrectScore() && false === $predictionValue->isCorrectScore()
            || $marketType->isDrawOrWin() && false === $predictionValue->isDrawOrWin()
        ) {
            $errors->addError(DomainErrors::PREDICTION_MARKET_TYPE_MUST_BE_COMPATIBLE_WITH_PREDICTION_VALUE);
            return null;
        }
        $createdAt = time();

        return new static($event, $marketType, $predictionValue, Status::unresolved(), $createdAt);
    }

    private function __construct(
        EventId $event,
        MarketType $marketType,
        PredictionValue $predictionValue,
        Status $status,
        int $createdAt
    ) {
        $this->event = $event;
        $this->marketType = $marketType;
        $this->predictionValue = $predictionValue;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->updatedAt = $createdAt;
    }

    public function setUpdateAt(int $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function changeStatus(Status $status): void
    {
        $this->status = $status;
        $this->updatedAt = time();
    }

    public function getEvent(): EventId
    {
        return $this->event;
    }

    public function getMarketType(): MarketType
    {
        return $this->marketType;
    }

    public function getPredictionValue(): PredictionValue
    {
        return $this->predictionValue;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): int
    {
        return $this->updatedAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
