<?php

declare(strict_types=1);

namespace App\Prediction\Domain\Repository;

use App\Prediction\Domain\Model\Prediction;

interface PredictionRepository
{
    public function save(Prediction $prediction): void;

    public function findById(int $id): ?Prediction;

    /**
     * @return Prediction[]
     */
    public function findAll(): array;
}
