<?php

declare(strict_types=1);

namespace App\Prediction\Domain;

class DomainErrors
{
    public const EVENT_ID_GREATER_THAN_ZERO = 'Event id must be greater than 0.';

    public const STATUS_NOT_SUPPORTED = 'Given prediction status is not supported';

    public const PREDICTION_VALUE_INVALID = 'Invalid prediction value.';

    public const PREDICTION_MARKET_TYPE_MUST_BE_COMPATIBLE_WITH_PREDICTION_VALUE = 'MarketType must be compatible with PredictionValue';

    public const NOT_NULL = 'Given value can not be null.';

    public const NOT_EMPTY = 'Given value can not be empty.';

    public const MARKET_TYPE_NOT_SUPPORTED = 'Given market type is not supported';
}
