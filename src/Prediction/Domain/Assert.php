<?php

declare(strict_types=1);

namespace App\Prediction\Domain;

use App\Prediction\Domain\Model\Errors;

class Assert
{
    public static function assertNotNull($value, Errors $errors): void
    {
        if (null !== $value) {
            return;
        }

        $errors->addError(DomainErrors::NOT_NULL);
    }

    public static function assertNotEmpty($value, Errors $errors): void
    {
        if (false === empty($value)) {
            return;
        }

        $errors->addError(DomainErrors::NOT_EMPTY);
    }
}
