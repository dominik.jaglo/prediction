<?php

declare(strict_types=1);

namespace App\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;

class MarketType
{
    public const DRAW_OR_WIN = '1x2';

    public const CORRECT_SCORE = 'correct_score';

    /**
     * @var string
     */
    private $type;

    public static function fromString(string $type, Errors $errors): ?self
    {
        if (!in_array($type, [self::DRAW_OR_WIN, self::CORRECT_SCORE])) {
            $errors->addError(DomainErrors::MARKET_TYPE_NOT_SUPPORTED);
            return null;
        }

        return new static($type);
    }

    public static function drawOrWin()
    {
        return new static(self::DRAW_OR_WIN);
    }

    public static function correctScore()
    {
        return new static(self::CORRECT_SCORE);
    }

    private function __construct(string $type)
    {
        $this->type = $type;
    }

    public function toString(): string
    {
        return $this->type;
    }

    public function isDrawOrWin(): bool
    {
        return self::DRAW_OR_WIN === $this->type;
    }

    public function isCorrectScore(): bool
    {
        return self::CORRECT_SCORE === $this->type;
    }
}
