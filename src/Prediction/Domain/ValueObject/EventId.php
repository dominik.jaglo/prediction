<?php

declare(strict_types=1);

namespace App\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;

class EventId
{
    /**
     * @var int
     */
    private $id;

    public static function fromInt(int $id, Errors $errors): ?self
    {
        if ($id < 1) {
            $errors->addError(DomainErrors::EVENT_ID_GREATER_THAN_ZERO);
            return null;
        }
        return new static($id);
    }

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public function toInt(): int
    {
        return $this->id;
    }
}
