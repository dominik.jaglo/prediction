<?php

declare(strict_types=1);

namespace App\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;

class PredictionValue
{
    public const HOME_TEAM_WIN = '1';

    public const DRAW = 'X';

    public const AWAY_TEAM_WIN = '2';

    /**
     * @var string
     */
    private $value;

    public static function fromString(string $value, Errors $errors): ?self
    {
        if (self::validateDrawOrWin($value) || self::validateCorrectScore($value)) {
            return new static($value);
        }

        $errors->addError(DomainErrors::PREDICTION_VALUE_INVALID);

        return null;
    }

    private static function validateDrawOrWin(string $value): bool
    {
        return in_array($value, [self::HOME_TEAM_WIN, self::DRAW, self::AWAY_TEAM_WIN]);
    }

    private static function validateCorrectScore(string $value): bool
    {
        return 1 === preg_match('/^\d+:\d+$/', $value);
    }

    public function isDrawOrWin(): bool
    {
        return static::validateDrawOrWin($this->value);
    }

    public function isCorrectScore(): bool
    {
        return static::validateCorrectScore($this->value);
    }

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public function toString(): string
    {
        return $this->value;
    }
}
