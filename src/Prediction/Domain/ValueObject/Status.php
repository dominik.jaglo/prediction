<?php

declare(strict_types=1);

namespace App\Prediction\Domain\ValueObject;

class Status
{
    public const WIN = 'win';
    public const LOST = 'lost';
    public const UNRESOLVED = 'unresolved';

    /**
     * @var string
     */
    private $value;

    public static function win(): self
    {
        return new static(self::WIN);
    }

    public static function lost(): self
    {
        return new static(self::LOST);
    }

    public static function unresolved(): self
    {
        return new static(self::UNRESOLVED);
    }

    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function isEqualTo(Status $other): bool
    {
        return $this->value === $other->value;
    }
}
