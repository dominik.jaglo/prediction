<?php

declare(strict_types=1);

namespace App\Prediction\Application\Dto;

interface Dto
{
    public function toArray(): array;
}
