<?php

declare(strict_types=1);

namespace App\Prediction\Application\Dto;

trait DtoTrait
{
    public function toArray(): array
    {
        return get_object_vars($this);
    }

    private function __construct()
    {
    }
}
