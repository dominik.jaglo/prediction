<?php

declare(strict_types=1);

namespace App\Prediction\Application\Dto;

class CollectionDto implements Dto
{
    /**
     * @var Dto[]
     */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function toArray(): array
    {
        return array_map(function (Dto $dto) {
            return $dto->toArray();
        }, $this->items);
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
