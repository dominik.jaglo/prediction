<?php

declare(strict_types=1);

namespace App\Prediction\Application\Dto;

use App\Prediction\Domain\Model\Prediction;

class PredictionDto implements Dto
{
    use DtoTrait;

    public static function create(Prediction $prediction): self
    {
        $dto = new static();
        $dto->id = $prediction->getId();
        $dto->event_id = $prediction->getEvent()->toInt();
        $dto->market_type = $prediction->getMarketType()->toString();
        $dto->prediction = $prediction->getPredictionValue()->toString();
        $dto->status = $prediction->getStatus()->toString();

        return $dto;
    }

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int
     */
    private $event_id;

    /**
     * @var string
     */
    private $market_type;

    /**
     * @var string
     */
    private $prediction;

    /**
     * @var string
     */
    private $status;
}
