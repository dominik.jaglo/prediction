<?php

declare(strict_types=1);

namespace App\Prediction\Application\Handler;

use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Application\Service\PredictionFactory;
use App\Prediction\Application\Service\TransactionManager;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Repository\PredictionRepository;

class CreatePredictionHandler
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var PredictionFactory
     */
    private $factory;

    /**
     * @var PredictionRepository
     */
    private $repository;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    public function __construct(
        Validator $validator,
        PredictionFactory $factory,
        PredictionRepository $repository,
        TransactionManager $transactionManager
    ) {
        $this->validator = $validator;
        $this->factory = $factory;
        $this->repository = $repository;
        $this->transactionManager = $transactionManager;
    }

    public function handle(CreatePredictionRequest $request, Errors $errors): void
    {
        if (false === $this->validator->validate($request, $errors)) {
            return;
        }

        $prediction = $this->factory->create($request, $errors);
        if (null === $prediction) {
            return;
        }

        $this->repository->save($prediction);
        $this->transactionManager->commit();
    }
}
