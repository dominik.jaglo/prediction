<?php

declare(strict_types=1);

namespace App\Prediction\Application\Handler;

use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Application\Service\StatusUpdater;
use App\Prediction\Application\Service\TransactionManager;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\Repository\PredictionRepository;

class UpdateStatusHandler
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var PredictionRepository
     */
    private $repository;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var StatusUpdater
     */
    private $statusUpdater;

    public function __construct(
        Validator $validator,
        StatusUpdater $statusUpdater,
        PredictionRepository $repository,
        TransactionManager $transactionManager
    ) {
        $this->validator = $validator;
        $this->repository = $repository;
        $this->transactionManager = $transactionManager;
        $this->statusUpdater = $statusUpdater;
    }

    public function handle(Prediction $prediction, UpdateStatusRequest $request, Errors $errors): void
    {
        if (false === $this->validator->validate($request, $errors)) {
            return;
        }

        $this->statusUpdater->update($prediction, $request, $errors);
        if (false === $errors->isEmpty()) {
            return;
        }

        $this->repository->save($prediction);
        $this->transactionManager->commit();
    }
}
