<?php

declare(strict_types=1);

namespace App\Prediction\Application\Service;

use App\Prediction\Domain\Model\Errors;

class ErrorsFactory
{
    public function create(): Errors
    {
        return new Errors();
    }
}
