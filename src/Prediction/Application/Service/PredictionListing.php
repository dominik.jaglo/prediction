<?php

declare(strict_types=1);

namespace App\Prediction\Application\Service;

use App\Prediction\Application\Dto\CollectionDto;
use App\Prediction\Application\Dto\PredictionDto;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\Repository\PredictionRepository;

class PredictionListing
{
    /**
     * @var PredictionRepository
     */
    private $repository;

    public function __construct(PredictionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getListing(): CollectionDto
    {
        return new CollectionDto(array_map(
            function (Prediction $prediction) {
                return PredictionDto::create($prediction);
            },
            $this->repository->findAll()
        ));
    }
}
