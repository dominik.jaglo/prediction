<?php

declare(strict_types=1);

namespace App\Prediction\Application\Service;

use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\ValueObject\EventId;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Domain\ValueObject\PredictionValue;

class PredictionFactory
{
    public function create(CreatePredictionRequest $request, Errors $errors): ?Prediction
    {
        $marketType = MarketType::fromString($request->getMarketType(), $errors);
        $eventId = EventId::fromInt($request->getEventId(), $errors);
        $prediction = PredictionValue::fromString($request->getPrediction(), $errors);

        if (false === $errors->isEmpty()) {
            return null;
        }

        return Prediction::create($eventId, $marketType, $prediction, $errors);
    }
}
