<?php

declare(strict_types=1);

namespace App\Prediction\Application\Service;

use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Domain\Assert;
use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\ValueObject\Status;

class StatusUpdater
{
    public function update(Prediction $prediction, UpdateStatusRequest $request, Errors $errors): void
    {
        $status = $request->getStatus();
        Assert::assertNotNull($status, $errors);
        Assert::assertNotEmpty($status, $errors);
        if (false === $errors->isEmpty()) {
            return;
        }

        $status = $this->createStatus($status, $errors);
        if (false === $errors->isEmpty()) {
            return;
        }

        $prediction->changeStatus($status);
    }

    private function createStatus(?string $status, Errors $errors): ?Status
    {
        if (Status::WIN === $status) {
            return Status::win();
        }

        if (Status::LOST === $status) {
            return Status::lost();
        }

        if (Status::UNRESOLVED === $status) {
            return Status::unresolved();
        }

        $errors->addError(DomainErrors::STATUS_NOT_SUPPORTED);
        return null;
    }
}
