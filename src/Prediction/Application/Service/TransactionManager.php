<?php

declare(strict_types=1);

namespace App\Prediction\Application\Service;

interface TransactionManager
{
    public function commit(): void;
}
