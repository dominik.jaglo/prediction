<?php

declare(strict_types=1);

namespace App\Prediction\Application\Validator;

use App\Prediction\Application\Request\Request;
use App\Prediction\Domain\Model\Errors;

interface Validator
{
    public function validate(Request $request, Errors $errors): bool;
}
