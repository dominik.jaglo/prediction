<?php

declare(strict_types=1);

namespace App\Prediction\Application;

class ApplicationErrors
{
    const JSON_CONTENT_TYPE = 'Content-type must be an application/json.';

    const JSON_INVALID_CONTENT = 'Sent invalid content.';

    const JSON_CONTENT_MUST_BE_AN_ARRAY = 'Sent json must be an array';
}
