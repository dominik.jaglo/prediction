<?php

declare(strict_types=1);

namespace App\Prediction\Application\Request;

class CreatePredictionRequest implements Request
{
    use RequestTrait;

    /**
     * @var int|null
     */
    private $event_id;

    /**
     * @var string|null
     */
    private $market_type;

    /**
     * @var string|null
     */
    private $prediction;

    public function getEventId()
    {
        return $this->event_id;
    }

    public function getMarketType()
    {
        return $this->market_type;
    }

    public function getPrediction()
    {
        return $this->prediction;
    }
}
