<?php

declare(strict_types=1);

namespace App\Prediction\Application\Request;

class UpdateStatusRequest implements Request
{
    use RequestTrait;

    /**
     * @var string|null
     */
    private $status;

    public function getStatus(): ?string
    {
        return $this->status;
    }
}
