<?php

declare(strict_types=1);

namespace App\Prediction\Application\Request;

use App\Prediction\Application\Model\Json;

interface Request
{
    /**
     * @return static
     */
    public static function create(Json $data): Request;
}
