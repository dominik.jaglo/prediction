<?php

declare(strict_types=1);

namespace App\Prediction\Application\Request;

use App\Prediction\Application\Model\Json;

trait RequestTrait
{
    /**
     * @return static
     */
    public static function create(Json $json): Request
    {
        $data = $json->toArray();
        $request = new static();
        foreach (array_keys(get_object_vars($request)) as $property) {
            $request->{$property} = $data[$property] ?? null;
        }

        return $request;
    }
}
