

### Instalation guide

- `git clone https://gitlab.com/dominik.jaglo/prediction.git`

-  enter project directory

-  install composer 

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

-  install vendors

```
./composer.phar install --ignore-platform-reqs
```

- run docker container
```
docker-compose up -d --build

```

- application runs on http://localhost:8080`

- run tests
```
docker-compose exec php-fpm bash

./vendor/bin/phpunit

```

- Postman collection is available in file `./postman_collection.json`
