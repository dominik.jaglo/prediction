<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\Model;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\ValueObject\EventId;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Domain\ValueObject\PredictionValue;
use App\Prediction\Domain\ValueObject\Status;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionTest extends TestCase
{
    /**
     * @dataProvider invalidValues
     */
    public function testCannotCreateForDifferentCorrectScoreAndDrawOrWin(MarketType $type, PredictionValue $value): void
    {
        $errors = new Errors();
        $prediction = Prediction::create(
            EventId::fromInt(1, $errors),
            $type,
            $value,
            $errors
        );

        static::assertNull($prediction);
        static::assertErrorsHaveMessage(
            DomainErrors::PREDICTION_MARKET_TYPE_MUST_BE_COMPATIBLE_WITH_PREDICTION_VALUE,
            $errors
        );
    }

    public function testCreate(): void
    {
        $prediction = $this->createPrediction();

        static::assertInstanceOf(Prediction::class, $prediction);
    }

    public function testCreatedPredictionHasUnresolvedStatus(): void
    {
        $prediction = $this->createPrediction();

        static::assertTrue(Status::unresolved()->isEqualTo($prediction->getStatus()));
    }

    public function testUpdateStatus(): void
    {
        $status = Status::win();
        $prediction = $this->createPrediction();

        $prediction->changeStatus($status);

        static::assertTrue($status->isEqualTo($prediction->getStatus()));
    }

    public function testUpdateStatusWillUpdateUpdatedAt(): void
    {
        $prediction = $this->createPrediction();
        $currentUpdatedAt = $prediction->getUpdatedAt();

        sleep(1);

        $prediction->changeStatus(Status::win());

        static::assertGreaterThan($currentUpdatedAt, $prediction->getUpdatedAt());
    }

    public function invalidValues(): array
    {
        $errors = new Errors();

        return [
            [MarketType::drawOrWin(), PredictionValue::fromString('1:1', $errors)],
            [MarketType::correctScore(), PredictionValue::fromString(PredictionValue::DRAW, $errors)],
        ];
    }
}
