<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\Model;

use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Test\TestCase;

class ErrorsTest extends TestCase
{
    public function testIsEmpty(): void
    {
        $errors = new Errors();

        static::assertTrue($errors->isEmpty());
    }

    public function testIsNotEmpty(): void
    {
        $errors = new Errors();
        $errors->addError('error');

        static::assertFalse($errors->isEmpty());
    }

    public function testToArrayEmptyErrors(): void
    {
        $errors = new Errors();

        static::assertSame([], $errors->toArray());
    }

    public function testToArrayWithErrors(): void
    {
        $errors = new Errors();

        $errors->addError('error');

        static::assertSame([['message' => 'error']], $errors->toArray());
    }

    public function testMerge(): void
    {
        $errors1 = new Errors();
        $errors1->addError('error-1');

        $errors2 = new Errors();
        $errors2->addError('error-2');

        $errors1->merge($errors2);

        static::assertSame([
            ['message' => 'error-1'],
            ['message' => 'error-2'],
        ], $errors1->toArray());
    }
}
