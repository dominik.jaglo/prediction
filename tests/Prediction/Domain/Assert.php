<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain;

use App\Prediction\Domain\Assert;
use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Test\TestCase;

class Assert extends TestCase
{
    public function testAssertNotNullAddError(): void
    {
        $errors = new Errors();

        Assert::assertNotNull(null, $errors);

        static::assertErrorsHaveMessage(DomainErrors::NOT_NULL, $errors);
    }

    public function testAssertNotNullWillNotAddErrors(): void
    {
        $errors = new Errors();

        Assert::assertNotNull('', $errors);

        static::assertTrue($errors->isEmpty());
    }

    public function testAssertNotEmptyAddError(): void
    {
        $errors = new Errors();

        Assert::assertNotEmpty('', $errors);

        static::assertErrorsHaveMessage(DomainErrors::NOT_EMPTY, $errors);
    }

    public function testAssertNotEmptyWillNotAddErrors(): void
    {
        $errors = new Errors();

        Assert::assertNotEmpty('valid', $errors);

        static::assertTrue($errors->isEmpty());
    }
}
