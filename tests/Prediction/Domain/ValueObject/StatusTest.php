<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\ValueObject;

use App\Prediction\Domain\ValueObject\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    public function testCreateWin(): void
    {
        $status = Status::win();

        static::assertSame(Status::WIN, $status->toString());
    }

    public function testCreateLost(): void
    {
        $status = Status::lost();

        static::assertSame(Status::LOST, $status->toString());
    }

    public function testCreateUnresolved(): void
    {
        $status = Status::unresolved();

        static::assertSame(Status::UNRESOLVED, $status->toString());
    }

    public function testIsEqualTo(): void
    {
        $status = Status::unresolved();

        static::assertTrue($status->isEqualTo($status));
    }

    public function testIsNotEqualTo(): void
    {
        $status1 = Status::unresolved();
        $status2 = Status::win();

        static::assertFalse($status1->isEqualTo($status2));
    }
}
