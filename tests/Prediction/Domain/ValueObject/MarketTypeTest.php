<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Infrastructure\Test\TestCase;

class MarketTypeTest extends TestCase
{
    public function testCreatePredefinedValue(): void
    {
        $type = MarketType::drawOrWin();

        static::assertSame(MarketType::DRAW_OR_WIN, $type->toString());
    }

    public function testCreateCorrectScore(): void
    {
        $type = MarketType::correctScore();

        static::assertSame(MarketType::CORRECT_SCORE, $type->toString());
    }

    public function testIsCorrectScore(): void
    {
        $type = MarketType::correctScore();

        static::assertTrue($type->isCorrectScore());
    }

    public function testIsDrawOrWin(): void
    {
        $type = MarketType::drawOrWin();

        static::assertTrue($type->isDrawOrWin());
    }

    public function testCreateFromString(): void
    {
        $type = MarketType::fromString(MarketType::CORRECT_SCORE, new Errors());

        static::assertInstanceOf(MarketType::class, $type);
    }

    public function testCannotCreateForNotSupportedValue(): void
    {
        $errors = new Errors();

        MarketType::fromString('test', $errors);

        static::assertErrorsHaveMessage(DomainErrors::MARKET_TYPE_NOT_SUPPORTED, $errors);
    }
}
