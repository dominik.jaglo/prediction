<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\ValueObject\PredictionValue;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionValueTest extends TestCase
{
    /**
     * @dataProvider validValues
     */
    public function testCreateWithValidValue(string $validValue): void
    {
        $errors = new Errors();
        $prediction = PredictionValue::fromString($validValue, $errors);

        static::assertInstanceOf(PredictionValue::class, $prediction);
        static::assertTrue($errors->isEmpty());
    }

    /**
     * @dataProvider invalidValues
     */
    public function testCannotCreateWithInvalidValues(string $invalidValue): void
    {
        $errors = new Errors();
        $prediction = PredictionValue::fromString($invalidValue, $errors);

        static::assertNull($prediction);
        static::assertErrorsHaveMessage(
            DomainErrors::PREDICTION_VALUE_INVALID,
            $errors
        );
    }

    /**
     * @dataProvider drawOrWinValues
     */
    public function testIsDrawOrWin($drawOrWin): void
    {
        $value = PredictionValue::fromString($drawOrWin, new Errors());

        static::assertTrue($value->isDrawOrWin());
    }

    /**
     * @dataProvider correctScore
     */
    public function testIsNotDrawOrWin($notDrawOrWin): void
    {
        $value = PredictionValue::fromString($notDrawOrWin, new Errors());

        static::assertFalse($value->isDrawOrWin());
    }

    /**
     * @dataProvider correctScore
     */
    public function testIsCorrectScore($correctScore): void
    {
        $value = PredictionValue::fromString($correctScore, new Errors());

        static::assertTrue($value->isCorrectScore());
    }

    /**
     * @dataProvider drawOrWinValues
     */
    public function testIsNotCorrectScore($notCorrectScore): void
    {
        $value = PredictionValue::fromString($notCorrectScore, new Errors());

        static::assertFalse($value->isCorrectScore());
    }

    public function validValues(): array
    {
        return [
            [PredictionValue::HOME_TEAM_WIN],
            [PredictionValue::AWAY_TEAM_WIN],
            [PredictionValue::DRAW],
            ['5:2'],
        ];
    }

    public function invalidValues(): array
    {
        return [
            ['B'],
            ['1:'],
            [':1'],
            ['X:1'],
            ['X:X'],
        ];
    }

    public function drawOrWinValues(): array
    {
        return [
            [PredictionValue::HOME_TEAM_WIN],
            [PredictionValue::AWAY_TEAM_WIN],
            [PredictionValue::DRAW],
        ];
    }

    public function correctScore(): array
    {
        return [
            ['1:1'],
            ['0:0'],
            ['2:1'],
        ];
    }
}
