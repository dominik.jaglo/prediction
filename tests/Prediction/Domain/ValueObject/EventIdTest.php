<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\ValueObject;

use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\ValueObject\EventId;
use App\Prediction\Infrastructure\Test\TestCase;

class EventIdTest extends TestCase
{
    public function testCreate(): void
    {
        $errors = new Errors();

        $id = EventId::fromInt(1, $errors);

        static::assertInstanceOf(EventId::class, $id);
        static::assertSame(1, $id->toInt());
    }

    public function testCannotCreate(): void
    {
        $errors = new Errors();

        $id = EventId::fromInt(0, $errors);

        static::assertNull($id);
        static::assertErrorsHaveMessage(
            DomainErrors::EVENT_ID_GREATER_THAN_ZERO,
            $errors
        );
    }
}
