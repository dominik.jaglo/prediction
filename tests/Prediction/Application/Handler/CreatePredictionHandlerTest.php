<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Handler;

use App\Prediction\Application\Handler\CreatePredictionHandler;
use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Application\Service\PredictionFactory;
use App\Prediction\Application\Service\TransactionManager;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\Repository\PredictionRepository;
use App\Prediction\Infrastructure\Test\TestCase;
use Mockery;

class CreatePredictionHandlerTest extends TestCase
{
    public function testWillNotCreatePredictionWhenValidationFails(): void
    {
        $errors = new Errors();
        $request = Mockery::mock(CreatePredictionRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnFalse();
        $factory = Mockery::mock(PredictionFactory::class);
        $factory->shouldNotReceive('create')->withAnyArgs()->never();
        $repository = Mockery::mock(PredictionRepository::class);
        $manager = Mockery::mock(TransactionManager::class);

        $handler = new CreatePredictionHandler($validator, $factory, $repository, $manager);

        $handler->handle($request, $errors);
    }

    public function testWillNotCreatePredictionWhenFactoryCausesErrors(): void
    {
        $errors = new Errors();
        $request = Mockery::mock(CreatePredictionRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnTrue();
        $factory = Mockery::mock(PredictionFactory::class);
        $factory->shouldNotReceive('create')->with($request, $errors)->andReturnNull()->once();
        $repository = Mockery::mock(PredictionRepository::class);
        $manager = Mockery::mock(TransactionManager::class);

        $handler = new CreatePredictionHandler($validator, $factory, $repository, $manager);

        $handler->handle($request, $errors);
    }

    public function testWillCreateAndSavePredictionForValidRequest(): void
    {
        $errors = new Errors();
        $prediction = Mockery::mock(Prediction::class);
        $request = Mockery::mock(CreatePredictionRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnTrue();
        $factory = Mockery::mock(PredictionFactory::class);
        $factory->shouldNotReceive('create')->with($request, $errors)->andReturn($prediction)->once();
        $repository = Mockery::mock(PredictionRepository::class);
        $repository->shouldReceive('save')->with($prediction)->once();
        $manager = Mockery::mock(TransactionManager::class);
        $manager->shouldReceive('commit')->once();

        $handler = new CreatePredictionHandler($validator, $factory, $repository, $manager);

        $handler->handle($request, $errors);
    }
}
