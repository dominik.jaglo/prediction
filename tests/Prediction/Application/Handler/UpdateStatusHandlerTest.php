<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Handler;

use App\Prediction\Application\Handler\UpdateStatusHandler;
use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Application\Service\StatusUpdater;
use App\Prediction\Application\Service\TransactionManager;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\Repository\PredictionRepository;
use App\Prediction\Infrastructure\Test\TestCase;
use Mockery;

class UpdateStatusHandlerTest extends TestCase
{
    public function testWhenValidationFailsWillNotUpdate(): void
    {
        $errors = new Errors();
        $request = Mockery::mock(UpdateStatusRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnFalse();
        $updater = Mockery::mock(StatusUpdater::class);
        $updater->shouldNotReceive('update')->withAnyArgs()->never();
        $repository = Mockery::mock(PredictionRepository::class);
        $manager = Mockery::mock(TransactionManager::class);
        $prediction = Mockery::mock(Prediction::class);

        $handler = new UpdateStatusHandler($validator, $updater, $repository, $manager);

        $handler->handle($prediction, $request, $errors);
    }

    public function testWhenUpdaterCausesErrorsWillNotIUpdatePrediction(): void
    {
        $prediction = Mockery::mock(Prediction::class);
        $errors = new Errors();
        $errors->addError('updateError');
        $request = Mockery::mock(UpdateStatusRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnTrue()->once();
        $updater = Mockery::mock(StatusUpdater::class);
        $updater->shouldReceive('update')->with($prediction, $request, $errors)->once();
        $repository = Mockery::mock(PredictionRepository::class);
        $manager = Mockery::mock(TransactionManager::class);

        $handler = new UpdateStatusHandler($validator, $updater, $repository, $manager);

        $handler->handle($prediction, $request, $errors);
    }

    public function testWillSavePredictionForValidRequest(): void
    {
        $prediction = Mockery::mock(Prediction::class);
        $errors = new Errors();
        $request = Mockery::mock(UpdateStatusRequest::class);
        $validator = Mockery::mock(Validator::class);
        $validator->shouldReceive('validate')->with($request, $errors)->andReturnTrue();
        $updater = Mockery::mock(StatusUpdater::class);
        $updater->shouldReceive('update')->with($prediction, $request, $errors);
        $repository = Mockery::mock(PredictionRepository::class);
        $repository->shouldReceive('save')->with($prediction)->once();
        $manager = Mockery::mock(TransactionManager::class);
        $manager->shouldReceive('commit')->once();

        $handler = new UpdateStatusHandler($validator, $updater, $repository, $manager);

        $handler->handle($prediction, $request, $errors);
    }
}
