<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Service;

use App\Prediction\Application\ApplicationErrors;
use App\Prediction\Application\Model\Json;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Service\JsonFactory;
use App\Prediction\Infrastructure\Test\TestCase;
use Symfony\Component\HttpFoundation\Request;

class JsonFactoryTest extends TestCase
{
    public function testCreateFromRequestWithValidJson(): void
    {
        $errors = new Errors();
        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getContentType')->andReturn('json');
        $request->shouldReceive('getContent')->with(false)->andReturn('{"a": 1}');
        $factory = new JsonFactory();

        $json = $factory->fromRequest($request, $errors);

        static::assertInstanceOf(Json::class, $json);
        static::assertSame(['a' => 1], $json->toArray());
    }

    public function testCannotCreateFromRequestWithInvalidContentType(): void
    {
        $errors = new Errors();
        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getContentType')->andReturn('xml');
        $factory = new JsonFactory();

        $json = $factory->fromRequest($request, $errors);

        static::assertNull($json);
        static::assertErrorsHaveMessage(ApplicationErrors::JSON_CONTENT_TYPE, $errors);
    }

    public function testCannotCreateFromRequestWithInvalidJson(): void
    {
        $errors = new Errors();
        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getContentType')->andReturn('json');
        $request->shouldReceive('getContent')->with(false)->andReturn('{"a": }');
        $factory = new JsonFactory();

        $json = $factory->fromRequest($request, $errors);

        static::assertNull($json);
        static::assertErrorsHaveMessage(ApplicationErrors::JSON_INVALID_CONTENT, $errors);
    }

    public function testCannotCreateFromRequestWithNotAnArray(): void
    {
        $errors = new Errors();
        $request = \Mockery::mock(Request::class);
        $request->shouldReceive('getContentType')->andReturn('json');
        $request->shouldReceive('getContent')->with(false)->andReturn('1');
        $factory = new JsonFactory();

        $json = $factory->fromRequest($request, $errors);

        static::assertNull($json);
        static::assertErrorsHaveMessage(ApplicationErrors::JSON_CONTENT_MUST_BE_AN_ARRAY, $errors);
    }
}
