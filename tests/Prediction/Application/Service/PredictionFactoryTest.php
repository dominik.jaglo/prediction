<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Service;

use App\Prediction\Application\Model\Json;
use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Application\Service\PredictionFactory;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Domain\Model\Prediction;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $request = CreatePredictionRequest::create(new Json([
            'event_id' => 1,
            'market_type' => MarketType::CORRECT_SCORE,
            'prediction' => '1:1',
        ]));
        $errors = new Errors();
        $factory = new PredictionFactory();

        $prediction = $factory->create($request, $errors);

        static::assertInstanceOf(Prediction::class, $prediction);
        static::assertSame(1, $prediction->getEvent()->toInt());
        static::assertSame(MarketType::CORRECT_SCORE, $prediction->getMarketType()->toString());
        static::assertSame('1:1', $prediction->getPredictionValue()->toString());
    }
}
