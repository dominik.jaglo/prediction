<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Service;

use App\Prediction\Application\Model\Json;
use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Application\Service\StatusUpdater;
use App\Prediction\Domain\DomainErrors;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Test\TestCase;

class StatusUpdaterTest extends TestCase
{
    public function testCannotCreateForNullStatus(): void
    {
        $errors = new Errors();
        $prediction = $this->createPrediction();
        $updater = new StatusUpdater();

        $request = UpdateStatusRequest::create(new Json(['status' => null]));

        $updater->update($prediction, $request, $errors);

        static::assertErrorsHaveMessage(DomainErrors::NOT_NULL, $errors);
    }

    public function testCannotCreateForEmptyString(): void
    {
        $errors = new Errors();
        $prediction = $this->createPrediction();
        $updater = new StatusUpdater();

        $request = UpdateStatusRequest::create(new Json(['status' => '']));

        $updater->update($prediction, $request, $errors);

        static::assertErrorsHaveMessage(DomainErrors::NOT_EMPTY, $errors);
    }

    public function testCannotCreateForNotSupportedStatus(): void
    {
        $errors = new Errors();
        $prediction = $this->createPrediction();
        $updater = new StatusUpdater();

        $request = UpdateStatusRequest::create(new Json(['status' => 'invalid']));

        $updater->update($prediction, $request, $errors);

        static::assertErrorsHaveMessage(DomainErrors::STATUS_NOT_SUPPORTED, $errors);
    }

    /**
     * @dataProvider validStatuses
     */
    public function testCreateValidStatus($status): void
    {
        $errors = new Errors();
        $prediction = $this->createPrediction();
        $updater = new StatusUpdater();

        $request = UpdateStatusRequest::create(new Json(['status' => $status]));

        $updater->update($prediction, $request, $errors);

        static::assertSame($status, $prediction->getStatus()->toString());
        static::assertTrue($errors->isEmpty());
    }

    public function validStatuses()
    {
        return [
            ['win'],
            ['lost'],
            ['unresolved'],
        ];
    }
}
