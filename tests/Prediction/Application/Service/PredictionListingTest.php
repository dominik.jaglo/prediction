<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Service;

use App\Prediction\Application\Service\PredictionListing;
use App\Prediction\Domain\Repository\PredictionRepository;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionListingTest extends TestCase
{
    public function testReturnEmptyCollection(): void
    {
        $repository = \Mockery::mock(PredictionRepository::class);
        $repository->shouldReceive('findAll')->andReturn([]);
        $listing = new PredictionListing($repository);

        $collection = $listing->getListing();

        static::assertSame([], $collection->toArray());
    }

    public function testReturnMappedPredictions(): void
    {
        $repository = \Mockery::mock(PredictionRepository::class);
        $repository->shouldReceive('findAll')->andReturn([$this->createPrediction()]);
        $listing = new PredictionListing($repository);

        $collection = $listing->getListing();

        static::assertSame([
            [
                'id' => null,
                'event_id' => 1,
                'market_type' => '1x2',
                'prediction' => 'X',
                'status' => 'unresolved',
            ],
        ], $collection->toArray());
    }
}
