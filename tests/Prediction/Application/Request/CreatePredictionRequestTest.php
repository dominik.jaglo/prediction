<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Request;

use App\Prediction\Application\Model\Json;
use App\Prediction\Application\Request\CreatePredictionRequest;
use App\Prediction\Domain\ValueObject\MarketType;
use App\Prediction\Infrastructure\Test\TestCase;

class CreatePredictionRequestTest extends TestCase
{
    public function testCreateWithoutValue(): void
    {
        $request = CreatePredictionRequest::create(new Json([]));

        static::assertNull($request->getPrediction());
        static::assertNull($request->getEventId());
        static::assertNull($request->getMarketType());
    }

    public function testCreateWithValue(): void
    {
        $request = CreatePredictionRequest::create(new Json([
            'event_id' => 1,
            'market_type' => MarketType::CORRECT_SCORE,
            'prediction' => '1:2',
        ]));

        static::assertSame(1, $request->getEventId());
        static::assertSame(MarketType::CORRECT_SCORE, $request->getMarketType());
        static::assertSame('1:2', $request->getPrediction());
    }
}
