<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Request;

use App\Prediction\Application\Model\Json;
use App\Prediction\Application\Request\UpdateStatusRequest;
use App\Prediction\Infrastructure\Test\TestCase;

class UpdateStatusRequestTest extends TestCase
{
    public function testCreateWithoutValue(): void
    {
        $request = UpdateStatusRequest::create(new Json([]));

        static::assertNull($request->getStatus());
    }

    public function testCreateWithValue(): void
    {
        $request = UpdateStatusRequest::create(new Json(['status' => 'win']));

        static::assertSame('win', $request->getStatus());
    }
}
