<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Dto;

use App\Prediction\Application\Dto\CollectionDto;
use App\Prediction\Application\Dto\Dto;
use App\Prediction\Infrastructure\Test\TestCase;

class CollectionDtoTest extends TestCase
{
    public function testToArray(): void
    {
        $dto = \Mockery::mock(Dto::class);
        $dto->shouldReceive('toArray')->andReturn(['id' => 1]);

        $collection = new CollectionDto([$dto]);

        static::assertSame([['id' => 1]], $collection->toArray());
    }

    public function testGetItems(): void
    {
        $dto = \Mockery::mock(Dto::class);
        $dto->shouldReceive('toArray')->andReturn(['id' => 1]);

        $collection = new CollectionDto([$dto]);

        static::assertSame([$dto], $collection->getItems());
    }
}
