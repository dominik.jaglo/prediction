<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Application\Dto;

use App\Prediction\Application\Dto\PredictionDto;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionDtoTest extends TestCase
{
    public function testStoredValues(): void
    {
        $prediction = $this->createPrediction();

        $dto = PredictionDto::create($prediction);

        static::assertSame(
            [
                'id' => null,
                'event_id' => 1,
                'market_type' => '1x2',
                'prediction' => 'X',
                'status' => 'unresolved',
            ],
            $dto->toArray()
        );
    }
}
