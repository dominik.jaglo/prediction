<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Domain\Model;

use App\Prediction\Application\Model\Json;
use App\Prediction\Infrastructure\Test\TestCase;

class JsonTest extends TestCase
{
    public function testCreate(): void
    {
        $expected = ['a' => 1];

        $json = new Json($expected);

        static::assertSame($expected, $json->toArray());
    }
}
