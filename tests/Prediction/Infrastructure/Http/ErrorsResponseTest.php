<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Infrastructure\Http;

use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Http\ErrorsResponse;
use App\Prediction\Infrastructure\Test\TestCase;

class ErrorsResponseTest extends TestCase
{
    public function testContainsBadRequestStatusCode(): void
    {
        $errors = new Errors();
        $errors->addError('error message');

        $response = ErrorsResponse::fromErrors($errors);

        static::assertSame(400, $response->getStatusCode());
    }

    public function testContainsJsonOfErrors(): void
    {
        $errors = new Errors();
        $errors->addError('error message');

        $response = ErrorsResponse::fromErrors($errors);

        static::assertSame('{"errors":[{"message":"error message"}]}', $response->getContent());
    }
}
