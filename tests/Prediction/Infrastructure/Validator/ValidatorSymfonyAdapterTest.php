<?php

declare(strict_types=1);

namespace App\Prediction\Infrastructure\Validator;

use App\Prediction\Application\Request\Request;
use App\Prediction\Application\Validator\Validator;
use App\Prediction\Domain\Model\Errors;
use App\Prediction\Infrastructure\Errors\ValidationToErrors;
use App\Prediction\Infrastructure\Test\TestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorSymfonyAdapterTest extends TestCase
{
    public function testA(): void
    {
        $symfonyValidator = \Mockery::mock(ValidatorInterface::class);

        $validator = new ValidatorSymfonyAdapter($symfonyValidator, new ValidationToErrors());

        static::assertInstanceOf(Validator::class, $validator);
    }

    public function testWhenValidationToErrorsReturnNotEmptyErrorsAdapterReturnsFalse(): void
    {
        $list = \Mockery::mock(ConstraintViolationListInterface::class);
        $request = \Mockery::mock(Request::class);
        $symfonyValidator = \Mockery::mock(ValidatorInterface::class);
        $symfonyValidator->shouldReceive('validate')->with($request)->andReturn($list);
        $validationToErrors = \Mockery::mock(ValidationToErrors::class);
        $errors = new Errors();
        $errors->addError('validationError');
        $validationToErrors->shouldReceive('toErrors')->with($list)->andReturn($errors);

        $validator = new ValidatorSymfonyAdapter($symfonyValidator, $validationToErrors);

        static::assertFalse($validator->validate($request, $errors));
    }

    public function testWhenValidationToErrorsReturnEmptyErrorsAdapterReturnsTrue(): void
    {
        $list = \Mockery::mock(ConstraintViolationListInterface::class);
        $request = \Mockery::mock(Request::class);
        $symfonyValidator = \Mockery::mock(ValidatorInterface::class);
        $symfonyValidator->shouldReceive('validate')->with($request)->andReturn($list);
        $validationToErrors = \Mockery::mock(ValidationToErrors::class);
        $errors = new Errors();
        $validationToErrors->shouldReceive('toErrors')->with($list)->andReturn($errors);

        $validator = new ValidatorSymfonyAdapter($symfonyValidator, $validationToErrors);

        static::assertTrue($validator->validate($request, $errors));
    }
}
