<?php

declare(strict_types=1);

namespace App\Tests\Prediction\Api;

use App\Prediction\Application\ApplicationErrors;
use App\Prediction\Domain\DomainErrors;
use App\Prediction\Infrastructure\Test\TestCase;

class PredictionApiTest extends TestCase
{
    public function testCreatePrediction(): void
    {
        static::resetMongo();

        $response = static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        static::assertSame(201, $response->getStatusCode());
    }

    public function testCannotCreatePredictionNonJsonRequest(): void
    {
        $response = static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => '',
            'market_type' => '',
            'prediction' => '',
        ], 'content/text');

        static::assertSame(400, $response->getStatusCode());
        static::assertSame(['errors' => [
            ['message' => ApplicationErrors::JSON_CONTENT_TYPE],
        ]], static::extractResponseData($response));
    }

    public function testCannotCreatePredictionForEmptyValues(): void
    {
        static::resetMongo();

        $response = static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => '',
            'market_type' => '',
            'prediction' => '',
        ]);
        static::assertSame(400, $response->getStatusCode());
        static::assertSame(['errors' => [
            ['message' => 'event_id - This value should not be blank.'],
            ['message' => 'event_id - This value should be of type integer.'],
            ['message' => 'market_type - This value should not be blank.'],
            ['message' => 'prediction - This value should not be blank.'],
        ]], static::extractResponseData($response));
    }

    public function testCannotCreateForDifferentCorrectScoreAndDrawOrWin(): void
    {
        static::resetMongo();

        $response = static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => 'X',
        ]);

        static::assertSame(400, $response->getStatusCode());
        static::assertSame([
            'errors' => [['message' => DomainErrors::PREDICTION_MARKET_TYPE_MUST_BE_COMPATIBLE_WITH_PREDICTION_VALUE]],
        ], static::extractResponseData($response));
    }

    public function testUpdateStatus(): void
    {
        static::resetMongo();
        static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        $putResponse = static::request('PUT', 'http://webserver/v1/predictions/1/status', [
            'status' => 'win',
        ]);
        $listingResponse = static::request('GET', 'http://webserver/v1/predictions');

        static::assertSame(204, $putResponse->getStatusCode());
        static::assertSame([
            'results' => [
                [
                    'id' => 1,
                    'event_id' => 1,
                    'market_type' => 'correct_score',
                    'prediction' => '3:2',
                    'status' => 'win',
                ],
            ],
        ], static::extractResponseData($listingResponse));
    }

    public function testCannotUpdateStatusWithInvalidStatus(): void
    {
        static::resetMongo();
        static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        $putResponse = static::request('PUT', 'http://webserver/v1/predictions/1/status', [
            'status' => 'invalid',
        ]);

        static::assertSame(400, $putResponse->getStatusCode());
        static::assertSame([
            'errors' => [['message' => DomainErrors::STATUS_NOT_SUPPORTED]],
        ], static::extractResponseData($putResponse));
    }

    public function testCannotUpdateStatusWithNonJsonRequest(): void
    {
        static::resetMongo();
        static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        $putResponse = static::request('PUT', 'http://webserver/v1/predictions/1/status', [
            'status' => 'invalid',
        ], 'content/text');

        static::assertSame(400, $putResponse->getStatusCode());
        static::assertSame(['errors' => [
            ['message' => ApplicationErrors::JSON_CONTENT_TYPE],
        ]], static::extractResponseData($putResponse));
    }

    public function testCannotUpdateStatusWithEmptyValue(): void
    {
        static::resetMongo();
        static::request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        $putResponse = static::request('PUT', 'http://webserver/v1/predictions/1/status', [
            'status' => '',
        ]);

        static::assertSame(400, $putResponse->getStatusCode());
        static::assertSame([
            'errors' => [['message' => 'status - This value should not be blank.']],
        ], static::extractResponseData($putResponse));
    }

    public function testUpdateStatusWillReturnNotFound(): void
    {
        static::resetMongo();

        $putResponse = static::request('PUT', 'http://webserver/v1/predictions/1/status', [
            'status' => 'win',
        ]);

        static::assertSame(404, $putResponse->getStatusCode());
    }

    public function testGetPredictions(): void
    {
        static::resetMongo();
        $this->request('POST', 'http://webserver/v1/predictions', [
            'event_id' => 1,
            'market_type' => 'correct_score',
            'prediction' => '3:2',
        ]);

        $listingResponse = static::request('GET', 'http://webserver/v1/predictions');

        static::assertSame(200, $listingResponse->getStatusCode());
        static::assertSame([
            'results' => [
                [
                    'id' => 1,
                    'event_id' => 1,
                    'market_type' => 'correct_score',
                    'prediction' => '3:2',
                    'status' => 'unresolved',
                ],
            ],
        ], static::extractResponseData($listingResponse));
    }
}
